# Remedial UTS PWL

Project template Remedial UTS Pemrograman Web Lanjutan.

## Aturan Remedial

- Maksimal waktu pengerjaan **1 Minggu**, dihitung dari waktu mulai remedial.

- Jawaban di-upload pada project gitlab *(public)* kalian masing-masing.

- Link project diberikan ke Dosen *(Edo Riansyah)* melalui pesan pribadi WA.

- Dilarang keras melakukan copy paste pekerjaan teman.

- Sebelum memulai remedial wajib membaca tahapan memulai dibawah ini terlebih dahulu.

## Tahapan Memulai

### 1. Download project

Klik link [download project](https://gitlab.com/web-lanjut-sttnf-20192/remedial-uts/-/archive/master/remedial-uts-master.zip) ini terlebih dahulu, setelah itu extract project ke folder **htdocs** (xampp) atau **html** (apache2 linux).

Ubah nama folder project menjadi **remedial-uts-pwl**.

### 2. Composer update

Lakukan composer update pada folder **remedial-uts-pwl**, dengan masuk ke folder remedial-uts-pwl terlebih dahulu melalui *Command Line*.

Untuk Windows

```
cd C:\xampp\htdocs\remedial-uts-pwl
```

Untuk Linux

```
cd /var/www/html/remedial-uts-pwl
```

Setelah itu jalankan

```
composer update
```

Jika belum meng-install *composer*, silakan di-install terlebih dahulu. Kunjungi [link ini](https://getcomposer.org/doc/00-intro.md#installation-windows).

### 3. Buat project baru

Pada akun gitlab kalian masing-masing buatlah project baru *(public)* dengan nama **remedial-uts-pwl**.

Project baru ini digunakan untuk mengumpulkan jawaban remedial UTS kalian.

### 4. Buat database & import

- Buatlah database **dbkoperasi** pada phpMyAdmin.
- Lakukan import file [**dbkoperasi.sql**](https://gitlab.com/web-lanjut-sttnf-20192/remedial-uts/-/blob/master/dbkoperasi.sql) yang ada pada template project ini ke database dbkoperasi. Gunakan phpMyAdmin!

Tahapan selesai, silakan kerjakan soalnya.

## Referensi link extension

- [Yii2 Widgets](http://demosbs3.krajee.com/widgets)
