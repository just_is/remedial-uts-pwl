<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\JenisProduk */

$this->title = 'Update Jenis Produk: ' . $model->idjenis;
$this->params['breadcrumbs'][] = ['label' => 'Jenis Produks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idjenis, 'url' => ['view', 'id' => $model->idjenis]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="jenis-produk-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
