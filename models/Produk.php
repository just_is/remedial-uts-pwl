<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "produk".
 *
 * @property int $idproduk
 * @property string|null $kode
 * @property string $nama
 * @property int|null $stok
 * @property int|null $min_stok
 * @property float|null $harga
 * @property int $idjenis
 *
 * @property JenisProduk $idjenis0
 * @property Pembelian[] $pembelians
 * @property PesananProduk[] $pesananProduks
 */
class Produk extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'produk';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama', 'idjenis'], 'required'],
            [['stok', 'min_stok', 'idjenis'], 'integer'],
            [['harga'], 'number'],
            [['kode'], 'string', 'max' => 6],
            [['nama'], 'string', 'max' => 45],
            [['kode'], 'unique'],
            [['idjenis'], 'exist', 'skipOnError' => true, 'targetClass' => JenisProduk::className(), 'targetAttribute' => ['idjenis' => 'idjenis']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idproduk' => 'Idproduk',
            'kode' => 'Kode',
            'nama' => 'Nama',
            'stok' => 'Stok',
            'min_stok' => 'Min Stok',
            'harga' => 'Harga',
            'idjenis' => 'Idjenis',
        ];
    }

    /**
     * Gets query for [[Idjenis0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdjenis0()
    {
        return $this->hasOne(JenisProduk::className(), ['idjenis' => 'idjenis']);
    }

    /**
     * Gets query for [[Pembelians]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPembelians()
    {
        return $this->hasMany(Pembelian::className(), ['idproduk' => 'idproduk']);
    }

    /**
     * Gets query for [[PesananProduks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPesananProduks()
    {
        return $this->hasMany(PesananProduk::className(), ['idproduk' => 'idproduk']);
    }
}
