<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jenis_produk".
 *
 * @property int $idjenis
 * @property string|null $nama
 *
 * @property Produk[] $produks
 */
class JenisProduk extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jenis_produk';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idjenis' => 'Idjenis',
            'nama' => 'Nama',
        ];
    }

    /**
     * Gets query for [[Produks]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduks()
    {
        return $this->hasMany(Produk::className(), ['idjenis' => 'idjenis']);
    }
}
