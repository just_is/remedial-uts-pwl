<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customer".
 *
 * @property int $idcustomer
 * @property string|null $kode
 * @property string|null $nama
 * @property string|null $gender
 * @property string|null $tmp_lahir
 * @property string|null $tgl_lahir
 * @property string|null $email
 * @property int $idkartu
 *
 * @property Kartu $idkartu0
 * @property Pesanan[] $pesanans
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tgl_lahir'], 'safe'],
            [['idkartu'], 'required'],
            [['idkartu'], 'integer'],
            [['kode'], 'string', 'max' => 10],
            [['nama', 'email'], 'string', 'max' => 45],
            [['gender'], 'string', 'max' => 1],
            [['tmp_lahir'], 'string', 'max' => 30],
            [['kode'], 'unique'],
            [['idkartu'], 'exist', 'skipOnError' => true, 'targetClass' => Kartu::className(), 'targetAttribute' => ['idkartu' => 'idkartu']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcustomer' => 'Idcustomer',
            'kode' => 'Kode',
            'nama' => 'Nama',
            'gender' => 'Gender',
            'tmp_lahir' => 'Tmp Lahir',
            'tgl_lahir' => 'Tgl Lahir',
            'email' => 'Email',
            'idkartu' => 'Idkartu',
        ];
    }

    /**
     * Gets query for [[Idkartu0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdkartu0()
    {
        return $this->hasOne(Kartu::className(), ['idkartu' => 'idkartu']);
    }

    /**
     * Gets query for [[Pesanans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPesanans()
    {
        return $this->hasMany(Pesanan::className(), ['idcustomer' => 'idcustomer']);
    }
}
