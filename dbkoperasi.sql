-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: dbkoperasi
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `idcustomer` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) DEFAULT NULL,
  `nama` varchar(45) DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  `tmp_lahir` varchar(30) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `idkartu` int(11) NOT NULL,
  PRIMARY KEY (`idcustomer`),
  UNIQUE KEY `kode_UNIQUE` (`kode`),
  KEY `fk_customer_kartu1_idx` (`idkartu`),
  CONSTRAINT `fk_customer_kartu1` FOREIGN KEY (`idkartu`) REFERENCES `kartu` (`idkartu`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (1,'C0001','Rosalie Naurah','P','Jakarta','2005-03-08','rosalie@yahoo.com',4),(2,'C0002','Alissa Khairunnisa','P','Jakarta','2005-11-16','alissa@gmail.com',2),(3,'C0003','Defghi Muhammad','L','Kebumen','2001-06-10','defghi@gmail.com',2),(4,'C0004','Faiz Fikri','L','Semarang','2009-08-17','faiz@hotmail.com',4),(5,'C0005','Iffa Nisrina','P','Kebumen','2004-08-17','iffa@gmail.com',1),(6,'C0006','Rahmi Maulida','P','Medan','2006-08-10','rahmi@nurulfikri.com',2),(7,'C0007','Agung Sedayu','L','Semarang','2000-01-18','agung@nurulfikri.com',4),(8,'C0008','Sekar Mirah','P','Yogyakarta','2002-05-14','mirah@gmail.com',4),(9,'C0009','Swandaru Geni','L','Denpasar','1998-02-12','geni@hotmail.com',1),(10,'C0010','Pandan Wangi','P','Jakarta','2001-03-22','wangi@icloud.com',2),(11,'C0011','Glagah Putih','L','Yogyakarta','2004-01-10','glagah@gmail.com',2),(12,'C0012','Rara Wulan','P','Yogyakarta','2005-04-14','wulan@gmail.com',1),(13,'C0013','Raden Rangga','L','Yogyakarta','2002-04-20','rangga@yahoo.com',2);
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jenis_produk`
--

DROP TABLE IF EXISTS `jenis_produk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jenis_produk` (
  `idjenis` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`idjenis`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jenis_produk`
--

LOCK TABLES `jenis_produk` WRITE;
/*!40000 ALTER TABLE `jenis_produk` DISABLE KEYS */;
INSERT INTO `jenis_produk` VALUES (1,'Elektronik'),(2,'Furniture'),(3,'Makanan'),(4,'Minuman'),(5,'Komputer');
/*!40000 ALTER TABLE `jenis_produk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kartu`
--

DROP TABLE IF EXISTS `kartu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kartu` (
  `idkartu` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(4) NOT NULL,
  `nama` varchar(45) DEFAULT NULL,
  `iuran` double DEFAULT NULL,
  `diskon` double DEFAULT NULL,
  PRIMARY KEY (`idkartu`),
  UNIQUE KEY `kode_UNIQUE` (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kartu`
--

LOCK TABLES `kartu` WRITE;
/*!40000 ALTER TABLE `kartu` DISABLE KEYS */;
INSERT INTO `kartu` VALUES (1,'BZ','Bronze',125000,0.05),(2,'SL','Silver',175000,0.1),(3,'GL','Gold',250000,0.2),(4,'PL','Platinum',400000,0.3);
/*!40000 ALTER TABLE `kartu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pembayaran`
--

DROP TABLE IF EXISTS `pembayaran`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pembayaran` (
  `idpembayaran` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime DEFAULT NULL,
  `jumlah` double DEFAULT NULL,
  `nokuitansi` varchar(10) NOT NULL,
  `idpesanan` int(11) NOT NULL,
  PRIMARY KEY (`idpembayaran`),
  UNIQUE KEY `nokuitansi_UNIQUE` (`nokuitansi`),
  KEY `fk_pembayaran_pesanan1_idx` (`idpesanan`),
  CONSTRAINT `fk_pembayaran_pesanan1` FOREIGN KEY (`idpesanan`) REFERENCES `pesanan` (`idpesanan`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pembayaran`
--

LOCK TABLES `pembayaran` WRITE;
/*!40000 ALTER TABLE `pembayaran` DISABLE KEYS */;
INSERT INTO `pembayaran` VALUES (1,'2013-08-10 00:00:00',500000,'W0001',1),(2,'2013-09-02 00:00:00',500000,'W0021',1);
/*!40000 ALTER TABLE `pembayaran` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pembelian`
--

DROP TABLE IF EXISTS `pembelian`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pembelian` (
  `idpembelian` int(11) NOT NULL AUTO_INCREMENT,
  `tgl` date DEFAULT NULL,
  `kode` varchar(10) NOT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `harga_beli` double DEFAULT NULL,
  `harga_jual` double DEFAULT NULL,
  `idproduk` int(11) NOT NULL,
  PRIMARY KEY (`idpembelian`),
  UNIQUE KEY `kode_UNIQUE` (`kode`),
  KEY `fk_pembelian_produk1_idx` (`idproduk`),
  CONSTRAINT `fk_pembelian_produk1` FOREIGN KEY (`idproduk`) REFERENCES `produk` (`idproduk`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pembelian`
--

LOCK TABLES `pembelian` WRITE;
/*!40000 ALTER TABLE `pembelian` DISABLE KEYS */;
INSERT INTO `pembelian` VALUES (1,'2013-08-01','Z0001-01',20,3000,3500,13),(2,'2013-08-01','Z0001-02',20,3000,3500,14),(3,'2013-08-01','Z0001-03',20,3000,3500,15),(4,'2013-08-21','Z0002-01',5,1450000,1550000,1),(5,'2013-08-21','Z0002-02',5,3200000,3600000,2),(6,'2013-08-21','Z0003-03',5,800000,1000000,3);
/*!40000 ALTER TABLE `pembelian` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pesanan`
--

DROP TABLE IF EXISTS `pesanan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pesanan` (
  `idpesanan` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` datetime DEFAULT NULL,
  `nomor` varchar(10) DEFAULT NULL,
  `total` double DEFAULT NULL,
  `idcustomer` int(11) NOT NULL,
  PRIMARY KEY (`idpesanan`),
  KEY `fk_pesanan_customer1_idx` (`idcustomer`),
  CONSTRAINT `fk_pesanan_customer1` FOREIGN KEY (`idcustomer`) REFERENCES `customer` (`idcustomer`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pesanan`
--

LOCK TABLES `pesanan` WRITE;
/*!40000 ALTER TABLE `pesanan` DISABLE KEYS */;
INSERT INTO `pesanan` VALUES (1,'2013-08-01 00:00:00','P0001',5150000,1),(2,'2013-08-01 00:00:00','P0002',NULL,2),(3,'2013-08-01 00:00:00','P0003',NULL,7),(4,'2013-08-02 00:00:00','P0004',NULL,2),(5,'2013-08-03 00:00:00','P0005',NULL,3),(6,'2013-08-04 00:00:00','P0006',NULL,4),(7,'2013-08-04 00:00:00','P0007',NULL,2),(8,'2013-09-01 00:00:00','P0008',NULL,3),(9,'2013-09-02 00:00:00','P0009',NULL,4);
/*!40000 ALTER TABLE `pesanan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pesanan_produk`
--

DROP TABLE IF EXISTS `pesanan_produk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pesanan_produk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idproduk` int(11) NOT NULL,
  `idpesanan` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `harga` double DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_produk_has_pesanan_pesanan1_idx` (`idpesanan`),
  KEY `fk_produk_has_pesanan_produk1_idx` (`idproduk`),
  CONSTRAINT `fk_produk_has_pesanan_pesanan1` FOREIGN KEY (`idpesanan`) REFERENCES `pesanan` (`idpesanan`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_produk_has_pesanan_produk1` FOREIGN KEY (`idproduk`) REFERENCES `produk` (`idproduk`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pesanan_produk`
--

LOCK TABLES `pesanan_produk` WRITE;
/*!40000 ALTER TABLE `pesanan_produk` DISABLE KEYS */;
INSERT INTO `pesanan_produk` VALUES (1,1,1,1,1400000),(2,2,1,1,3750000),(3,1,2,1,1450000),(4,6,3,5,1500),(5,7,3,3,4500),(6,13,4,1,3500),(7,14,4,1,3500),(8,15,4,1,3500),(9,9,5,3,2000),(10,11,5,2,8500),(11,1,6,1,1500000),(12,2,7,1,3800000),(13,3,8,1,890000),(14,4,9,1,4800000);
/*!40000 ALTER TABLE `pesanan_produk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produk`
--

DROP TABLE IF EXISTS `produk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produk` (
  `idproduk` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(6) DEFAULT NULL,
  `nama` varchar(45) NOT NULL,
  `stok` int(11) DEFAULT NULL,
  `min_stok` int(11) DEFAULT NULL,
  `harga` double DEFAULT NULL,
  `idjenis` int(11) NOT NULL,
  PRIMARY KEY (`idproduk`),
  UNIQUE KEY `kode_UNIQUE` (`kode`),
  KEY `fk_produk_jenis_produk_idx` (`idjenis`),
  CONSTRAINT `fk_produk_jenis_produk` FOREIGN KEY (`idjenis`) REFERENCES `jenis_produk` (`idjenis`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produk`
--

LOCK TABLES `produk` WRITE;
/*!40000 ALTER TABLE `produk` DISABLE KEYS */;
INSERT INTO `produk` VALUES (1,'TV01','Televisi 21 inch',20,3,1440000,1),(2,'K001','Kulkas 2 Pintu',10,3,3780000,1),(3,'K002','Kulkas Mini',15,3,900000,1),(4,'S001','Sofa Tamu',8,1,4320000,2),(5,'M001','Meja Makan',21,3,1020000,2),(6,'MI01','Mie Instan',100,20,1500,3),(7,'TE01','Teh Kotak',30,20,4800,4),(8,'G001','Gula Pasir',40,10,7800,3),(9,'KP01','Kopi Jahe',5,10,2000,3),(10,'SU01','Susu Bubuk',30,10,42500,3),(11,'SU02','Susu Coklat',35,10,8700,3),(12,'SU03','Susu Kental Manis',100,20,8500,3),(13,'CO01','Cola Red',30,20,3500,4),(14,'CO02','Cola Orange',10,20,3500,4),(15,'CO03','Cola Coke',40,20,3500,4);
/*!40000 ALTER TABLE `produk` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-04 11:08:30
